package com.easyBucketSoft.prodigy;


import com.easyBucketSoft.prodigy.database.model.*;
import com.easyBucketSoft.prodigy.database.repository.*;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;


@Component @Transactional
public class ProdigySeeder implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProdigySeeder.class);

    private final UserRepository userRepository;
    private final TeamRepository teamRepository;
    private final PlayerRepository playerRepository;
    private final CoachRepository coachRepository;
    private final RefereeRepository refereeRepository;
    private final ContractRepository contractRepository;
    private final EmploymentRepository employmentRepository;

    public ProdigySeeder(UserRepository userRepository, TeamRepository teamRepository, PlayerRepository playerRepository,
                         CoachRepository coachRepository, RefereeRepository refereeRepository, ContractRepository contractRepository, EmploymentRepository employmentRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.teamRepository = teamRepository;
        this.playerRepository = playerRepository;
        this.coachRepository = coachRepository;
        this.refereeRepository = refereeRepository;
        this.contractRepository = contractRepository;
        this.employmentRepository = employmentRepository;
    }

    @Override
    public void run(String... args) {
        var globalDate = LocalDate.of(2020, 8, 1);
        var globalTime = LocalTime.of(10, 0);
        var globalInit = LocalDateTime.of(globalDate, globalTime);
        LOGGER.info("---------------------------------------------------------------------------------------------------");
        var users = seedUsers(globalInit.plusDays(0),               LocalDateTime.now());
        var teams = seedTeams(globalInit.plusDays(5),               LocalDateTime.now());
        var players = seedPlayers(globalInit.plusDays(10),          LocalDateTime.now());
        var coaches = seedCoaches(globalInit.plusDays(15),          LocalDateTime.now());
        var referees = seedReferees(globalInit.plusDays(20),        LocalDateTime.now());
        var contracts   = seedContracts(globalInit.plusDays(25),    LocalDateTime.now());
        var employments = seedEmployments(globalInit.plusDays(25),  LocalDateTime.now());
        LOGGER.info("---------------------------------------------------------------------------------------------------");

        contracts.get(0).deactivate();
        employments.get(0).deactivate();
    }

    private List<User> seedUsers(LocalDateTime init, LocalDateTime start) {
        var users = userRepository.findAll();
        users.forEach(u -> {
            u.setCreatedAt(init);
            u.getContact().setCreatedAt(init);
        });
        var savedUsers = userRepository.saveAll(users);
        LOGGER.info("Seeding users finished [elapsed: {} ms, entries: {}]",
            ChronoUnit.MILLIS.between(start, LocalDateTime.now()),
            savedUsers.size()
        );
        return savedUsers;
    }

    private List<Team> seedTeams(LocalDateTime init, LocalDateTime start) {
        var teams = teamRepository.findAll();
        teams.forEach(t -> {
            t.setCreatedAt(init);
            t.getCourt().setCreatedAt(init);
        });

        var savedTeams = teamRepository.saveAll(teams);
        LOGGER.info("Seeding teams finished [elapsed: {} ms, entries: {}]",
            ChronoUnit.MILLIS.between(start, LocalDateTime.now()),
            savedTeams.size()
        );
        return savedTeams;
    }

    private List<Player> seedPlayers(LocalDateTime init, LocalDateTime start) {
        var players = playerRepository.findAll();
        players.forEach(p -> {
            p.setCreatedAt(init);
        });
        var savedPlayers = playerRepository.saveAll(players);
        LOGGER.info("Seeding players finished [elapsed: {} ms, entries: {}]",
            ChronoUnit.MILLIS.between(start, LocalDateTime.now()),
            savedPlayers.size()
        );
        return savedPlayers;
    }

    private List<Coach> seedCoaches(LocalDateTime init, LocalDateTime start) {
        var coaches = coachRepository.findAll();
        coaches.forEach(c -> {
            c.setCreatedAt(init);
        });
        var savedCoaches = coachRepository.saveAll(coaches);
        LOGGER.info("Seeding coaches finished [elapsed: {} ms, entries: {}]",
            ChronoUnit.MILLIS.between(start, LocalDateTime.now()),
            savedCoaches.size()
        );
        return savedCoaches;
    }

    private List<Referee> seedReferees(LocalDateTime init, LocalDateTime start) {
        var referees = refereeRepository.findAll();
        referees.forEach(r -> {
            r.setCreatedAt(init);
        });
        var savedReferees = refereeRepository.saveAll(referees);
        LOGGER.info("Seeding referees finished [elapsed: {} ms, entries: {}]",
            ChronoUnit.MILLIS.between(start, LocalDateTime.now()),
            savedReferees.size()
        );
        return savedReferees;
    }

    private List<Contract> seedContracts(LocalDateTime init, LocalDateTime start) {
        var contracts = contractRepository.findAll();
        contracts.forEach(c -> {
            c.setStart(init.toLocalDate());
            c.setFinish(LocalDate.now().plusYears(1));
            c.setCreatedAt(init);
            c.activate();
        });
        var savedContracts = contractRepository.saveAll(contracts);
        LOGGER.info("Seeding contracts finished [elapsed: {} ms, entries: {}]",
            ChronoUnit.MILLIS.between(start, LocalDateTime.now()),
            savedContracts.size()
        );
        return savedContracts;
    }

    private List<Employment> seedEmployments(LocalDateTime init, LocalDateTime start) {
        var employments = employmentRepository.findAll();
        employments.forEach(e -> {
            e.setStart(init.toLocalDate());
            e.setFinish(LocalDate.now().plusYears(1));
            e.setCreatedAt(init);
            e.activate();
        });
        var savedEmployments = employmentRepository.saveAll(employments);
        LOGGER.info("Seeding employments finished [elapsed: {} ms, entries: {}]",
            ChronoUnit.MILLIS.between(start, LocalDateTime.now()),
            savedEmployments.size()
        );
        return savedEmployments;

    }


}