package com.easyBucketSoft.prodigy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

import java.util.Locale;

@SpringBootApplication
@ConfigurationPropertiesScan
public class ProdigyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProdigyApplication.class, args);
        Locale.setDefault(new Locale("en", "US"));
    }

}
