package com.easyBucketSoft.prodigy.api.data.mapper;

import com.easyBucketSoft.prodigy.api.data.payload.coach.CoachCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.coach.CoachDTO;
import com.easyBucketSoft.prodigy.database.model.Coach;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class CoachMapper {

    public abstract List<CoachDTO> mapCoaches(List<Coach> coachs);

    @Mapping(target = "contact.createdAt", ignore = true)
    @Mapping(target = "contact.updatedAt", ignore = true)
    public abstract CoachDTO mapCoach(Coach coach);

    public abstract Coach createCoach(CoachCreateDTO coachCreateDTO);

    public abstract void updateCoach(@MappingTarget Coach coach, CoachCreateDTO coachCreateDTO);


}