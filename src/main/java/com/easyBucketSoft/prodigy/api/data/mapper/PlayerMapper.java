package com.easyBucketSoft.prodigy.api.data.mapper;

import com.easyBucketSoft.prodigy.api.data.payload.player.PlayerCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.player.PlayerDTO;
import com.easyBucketSoft.prodigy.database.model.Player;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class PlayerMapper {

    public abstract List<PlayerDTO> mapPlayers(List<Player> players);

    public abstract PlayerDTO mapPlayer(Player player);

    public abstract Player createPlayer(PlayerCreateDTO playerCreateDTO);

    public abstract void updatePlayer(@MappingTarget Player player, PlayerCreateDTO playerCreateDTO);

}