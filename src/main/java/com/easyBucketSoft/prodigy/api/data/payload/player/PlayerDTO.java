package com.easyBucketSoft.prodigy.api.data.payload.player;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public record PlayerDTO(Long id, String name, Short height, Short weight, LocalDate birth, String createdBy, LocalDateTime createdAt, LocalDateTime updatedAt) {}

