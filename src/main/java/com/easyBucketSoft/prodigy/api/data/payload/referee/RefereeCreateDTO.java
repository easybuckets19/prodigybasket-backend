package com.easyBucketSoft.prodigy.api.data.payload.referee;

import com.easyBucketSoft.prodigy.api.data.payload.contact.ContactCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.contact.ContactDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import org.hibernate.validator.constraints.Length;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record RefereeCreateDTO(
    @NotBlank @Length(min = 2, max = 72)
    String name,
    @NotNull @Valid
    ContactCreateDTO contact
) {}