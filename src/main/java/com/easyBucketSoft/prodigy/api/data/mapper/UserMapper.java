package com.easyBucketSoft.prodigy.api.data.mapper;

import com.easyBucketSoft.prodigy.api.data.payload.user.UserCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.user.UserDTO;
import com.easyBucketSoft.prodigy.database.model.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class UserMapper {

    public abstract List<UserDTO> mapUsers(List<User> users);

    @Mapping(target = "contact.createdAt", ignore = true)
    @Mapping(target = "contact.updatedAt", ignore = true)
    public abstract UserDTO mapUser(User user);

    public abstract User createUser(UserCreateDTO userCreateDTO);

    public abstract void updateUser(@MappingTarget User user, UserCreateDTO userCreateDTO);

}
