package com.easyBucketSoft.prodigy.api.data.mapper;

import com.easyBucketSoft.prodigy.api.data.payload.team.TeamCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.team.TeamDTO;
import com.easyBucketSoft.prodigy.database.model.Team;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class TeamMapper {

    public abstract List<TeamDTO> mapTeams(List<Team> teams);

    @Mapping(target = "contact",            source = "owner.contact")
    @Mapping(target = "owner.createdAt",    ignore = true)
    @Mapping(target = "owner.updatedAt",    ignore = true)
    @Mapping(target = "owner.contact",      ignore = true)
    @Mapping(target = "court.createdAt",    ignore = true)
    @Mapping(target = "court.updatedAt",    ignore = true)
    public abstract TeamDTO mapTeam(Team team);

    public abstract Team createTeam(TeamCreateDTO teamCreateDTO);

    public abstract void updateTeam(@MappingTarget Team team, TeamCreateDTO teamCreateDTO);

}
