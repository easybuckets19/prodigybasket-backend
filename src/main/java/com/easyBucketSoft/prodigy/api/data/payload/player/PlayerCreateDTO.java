package com.easyBucketSoft.prodigy.api.data.payload.player;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record PlayerCreateDTO(
    @NotBlank @Length(min = 2, max = 72)
    String name,
    @NotNull @Positive
    Short height,
    @NotNull @Positive
    Short weight,
    @NotNull @Past
    LocalDate birth
) {}
