package com.easyBucketSoft.prodigy.api.data.payload.court;

import com.easyBucketSoft.prodigy.database.embeddable.Address;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.Builder;
import org.hibernate.validator.constraints.Length;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record CourtCreateDTO(
    @NotBlank @Length(min = 2, max = 72)
    String name,
    @NotNull @Min(1) @Max(5)
    Short rating,
    @NotNull @Positive
    Integer capacity,
    @NotNull @Valid
    Address address
) {}