package com.easyBucketSoft.prodigy.api.data.payload.court;

import com.easyBucketSoft.prodigy.database.embeddable.Address;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public record CourtDTO(Long id, String name, Short rating, Integer capacity, Address address, LocalDateTime createdAt, LocalDateTime updatedAt) {}
