package com.easyBucketSoft.prodigy.api.data.payload.employment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

import java.time.LocalDate;

import static com.easyBucketSoft.prodigy.database.model.Employment.Position;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record EmploymentCreateDTO(
    @NotNull @Future
    LocalDate start,
    @NotNull @Future
    LocalDate finish,
    @NotNull
    Position position,
    @NotNull
    Long team,
    @NotNull
    Long coach
) { }