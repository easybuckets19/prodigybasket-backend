package com.easyBucketSoft.prodigy.api.data.payload.contract;

import com.easyBucketSoft.prodigy.api.data.payload.player.PlayerDTO;
import com.easyBucketSoft.prodigy.api.data.payload.team.TeamDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.easyBucketSoft.prodigy.database.model.Contract.Position;

@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public record ContractDTO(Long id, LocalDate start, LocalDate finish, Boolean isActive, Short jersey, Position position, TeamDTO team, PlayerDTO player, LocalDateTime createdAt, LocalDateTime updatedAt) { }