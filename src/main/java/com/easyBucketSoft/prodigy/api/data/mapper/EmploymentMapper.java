package com.easyBucketSoft.prodigy.api.data.mapper;

import com.easyBucketSoft.prodigy.api.data.payload.employment.EmploymentCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.employment.EmploymentDTO;
import com.easyBucketSoft.prodigy.database.model.Coach;
import com.easyBucketSoft.prodigy.database.model.Employment;
import com.easyBucketSoft.prodigy.database.model.Team;
import com.easyBucketSoft.prodigy.database.repository.CoachRepository;
import com.easyBucketSoft.prodigy.database.repository.TeamRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class EmploymentMapper {
    private CoachRepository coachRepository;
    private TeamRepository teamRepository;

    public abstract List<EmploymentDTO> mapEmployments(List<Employment> employments);

    @Mapping(target = "team.owner",         ignore = true)
    @Mapping(target = "team.court",         ignore = true)
    @Mapping(target = "team.createdAt",     ignore = true)
    @Mapping(target = "team.updatedAt",     ignore = true)
    @Mapping(target = "coach.createdAt",    ignore = true)
    @Mapping(target = "coach.updatedAt",    ignore = true)
    public abstract EmploymentDTO mapEmployment(Employment employment);

    public abstract Employment createEmployment(EmploymentCreateDTO employmentCreateDTO);

    public abstract void updateEmployment(@MappingTarget Employment employment, EmploymentCreateDTO employmentCreateDTO);

    protected Coach resolveCoach(Long id) {
        return coachRepository.getReferenceById(id);
    }

    protected Team resolveTeam(Long id) {
        return teamRepository.getReferenceById(id);
    }

    @Autowired
    public void setCoachRepository(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    @Autowired
    public void setTeamRepository(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

}