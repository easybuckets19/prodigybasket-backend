package com.easyBucketSoft.prodigy.api.data.payload.team;

import com.easyBucketSoft.prodigy.api.data.payload.court.CourtCreateDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import org.hibernate.validator.constraints.Length;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record TeamCreateDTO(
    @NotBlank @Length(min = 2, max = 72)
    String name,
    @Valid
    CourtCreateDTO court
) {}