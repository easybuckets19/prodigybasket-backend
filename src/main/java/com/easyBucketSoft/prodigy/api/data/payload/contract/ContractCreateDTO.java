package com.easyBucketSoft.prodigy.api.data.payload.contract;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

import java.time.LocalDate;

import static com.easyBucketSoft.prodigy.database.model.Contract.Position;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record ContractCreateDTO(
    @NotNull @Future
    LocalDate start,
    @NotNull @Future
    LocalDate finish,
    @NotNull @Min(0)
    Short jersey,
    @NotNull
    Position position,
    @NotNull
    Long team,
    @NotNull
    Long player
) { }