package com.easyBucketSoft.prodigy.api.data.payload.user;

import com.easyBucketSoft.prodigy.api.data.payload.contact.ContactCreateDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import org.hibernate.validator.constraints.Length;

import static com.easyBucketSoft.prodigy.database.model.User.Role;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record UserCreateDTO(
    @NotBlank @Length(min = 6, max = 18)
    String username,
    @NotBlank @Length(min = 6, max = 18)
    String password,
    @NotBlank @Length(min = 2, max = 72)
    String name,
    @NotNull
    Role role,
    @NotNull @Valid
    ContactCreateDTO contact
) {}
