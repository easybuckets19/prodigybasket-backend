package com.easyBucketSoft.prodigy.api.data.payload.coach;

import com.easyBucketSoft.prodigy.api.data.payload.contact.ContactDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public record CoachDTO(Long id, String name, ContactDTO contact, String createdBy, LocalDateTime createdAt, LocalDateTime updatedAt) {}