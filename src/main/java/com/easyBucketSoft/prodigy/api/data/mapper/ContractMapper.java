package com.easyBucketSoft.prodigy.api.data.mapper;

import com.easyBucketSoft.prodigy.api.data.payload.contract.ContractCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.contract.ContractDTO;
import com.easyBucketSoft.prodigy.database.model.Contract;
import com.easyBucketSoft.prodigy.database.model.Player;
import com.easyBucketSoft.prodigy.database.model.Team;
import com.easyBucketSoft.prodigy.database.repository.PlayerRepository;
import com.easyBucketSoft.prodigy.database.repository.TeamRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class ContractMapper {
    private PlayerRepository playerRepository;
    private TeamRepository teamRepository;

    public abstract List<ContractDTO> mapContracts(List<Contract> contracts);

    @Mapping(target = "team.owner",         ignore = true)
    @Mapping(target = "team.court",         ignore = true)
    @Mapping(target = "team.createdAt",     ignore = true)
    @Mapping(target = "team.updatedAt",     ignore = true)
    @Mapping(target = "player.createdAt",   ignore = true)
    @Mapping(target = "player.updatedAt",   ignore = true)
    public abstract ContractDTO mapContract(Contract contract);

    public abstract Contract createContract(ContractCreateDTO contractCreateDTO);

    public abstract void updateContract(@MappingTarget Contract contract, ContractCreateDTO contractCreateDTO);

    protected Player resolvePlayer(Long id) {
        return playerRepository.getReferenceById(id);
    }

    protected Team resolveTeam(Long id) {
        return teamRepository.getReferenceById(id);
    }

    @Autowired
    public void setPlayerRepository(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Autowired
    public void setTeamRepository(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

}