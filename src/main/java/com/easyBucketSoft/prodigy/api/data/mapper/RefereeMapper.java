package com.easyBucketSoft.prodigy.api.data.mapper;

import com.easyBucketSoft.prodigy.api.data.payload.referee.RefereeCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.referee.RefereeDTO;
import com.easyBucketSoft.prodigy.database.model.Referee;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class RefereeMapper {

    public abstract List<RefereeDTO> mapReferees(List<Referee> referees);

    @Mapping(target = "contact.createdAt", ignore = true)
    @Mapping(target = "contact.updatedAt", ignore = true)
    public abstract RefereeDTO mapReferee(Referee referee);

    public abstract Referee createReferee(RefereeCreateDTO refereeCreateDTO);

    public abstract void updateReferee(@MappingTarget Referee referee, RefereeCreateDTO refereeCreateDTO);

}