package com.easyBucketSoft.prodigy.api.data.payload.employment;

import com.easyBucketSoft.prodigy.api.data.payload.coach.CoachDTO;
import com.easyBucketSoft.prodigy.api.data.payload.team.TeamDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.easyBucketSoft.prodigy.database.model.Employment.Position;

@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public record EmploymentDTO(Long id, LocalDate start, LocalDate finish, Boolean active, Short jersey, Position position, TeamDTO team, CoachDTO coach, LocalDateTime createdAt, LocalDateTime updatedAt) {}