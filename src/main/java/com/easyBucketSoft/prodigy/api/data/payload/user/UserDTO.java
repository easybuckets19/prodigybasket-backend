package com.easyBucketSoft.prodigy.api.data.payload.user;

import com.easyBucketSoft.prodigy.api.data.payload.contact.ContactDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

import java.time.LocalDateTime;

import static com.easyBucketSoft.prodigy.database.model.User.Role;
import static com.easyBucketSoft.prodigy.database.model.User.Status;

@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public record UserDTO(Long id, String username, String name, Role role, Status status, ContactDTO contact, LocalDateTime createdAt, LocalDateTime updatedAt) {}

