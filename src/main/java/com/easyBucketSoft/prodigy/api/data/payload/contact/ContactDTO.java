package com.easyBucketSoft.prodigy.api.data.payload.contact;

import com.easyBucketSoft.prodigy.database.embeddable.Address;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public record ContactDTO(String email, String phone, Address address, LocalDateTime createdAt, LocalDateTime updatedAt) {}
