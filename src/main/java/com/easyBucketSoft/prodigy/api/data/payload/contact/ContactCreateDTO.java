package com.easyBucketSoft.prodigy.api.data.payload.contact;

import com.easyBucketSoft.prodigy.database.embeddable.Address;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record ContactCreateDTO(
    @NotBlank @Email
    String email,
    @NotBlank
    String phone,
    @NotNull @Valid
    Address address
) {}
