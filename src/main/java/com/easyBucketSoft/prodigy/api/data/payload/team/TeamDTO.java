package com.easyBucketSoft.prodigy.api.data.payload.team;

import com.easyBucketSoft.prodigy.api.data.payload.contact.ContactDTO;
import com.easyBucketSoft.prodigy.api.data.payload.court.CourtDTO;
import com.easyBucketSoft.prodigy.api.data.payload.user.UserDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public record TeamDTO(Long id, String name, ContactDTO contact, UserDTO owner, CourtDTO court, LocalDateTime createdAt, LocalDateTime updatedAt) {}