package com.easyBucketSoft.prodigy.api.controller;

import com.easyBucketSoft.prodigy.api.data.payload.team.TeamCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.team.TeamCreateDTO;
import com.easyBucketSoft.prodigy.core.service.contract.TeamService;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TeamController {
    private final TeamService teamService;

    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @GetMapping(value = "/teams")
    protected ResponseEntity<?> getTeams() {
        var teams = teamService.getTeams();
        return new ResponseEntity<>(teams, HttpStatus.OK);
    }

    @PostMapping(value = "/teams")
    protected ResponseEntity<?> createTeam(@RequestBody TeamCreateDTO teamCreateDTO) {
        var team = teamService.createTeam(teamCreateDTO);
        return new ResponseEntity<>(team, HttpStatus.CREATED);
    }

    @GetMapping(value = "/teams/{id}")
    protected ResponseEntity<?> getTeam(@PathVariable Long id) {
        var team = teamService.getTeam(id);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }

    @PatchMapping(value = "/teams/{id}")
    protected ResponseEntity<?> updateTeam(@PathVariable Long id, @RequestBody TeamCreateDTO teamUpdateDTO) {
        var team = teamService.updateTeam(id, teamUpdateDTO);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }
}
