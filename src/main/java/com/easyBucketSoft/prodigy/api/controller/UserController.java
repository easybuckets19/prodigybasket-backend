package com.easyBucketSoft.prodigy.api.controller;

import com.easyBucketSoft.prodigy.api.data.payload.user.UserCreateDTO;
import com.easyBucketSoft.prodigy.core.service.contract.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/users")
    protected ResponseEntity<?> getUsers() {
        var users = userService.getUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PostMapping(value = "/users")
    protected ResponseEntity<?> createUser(@RequestBody UserCreateDTO userCreateDTO) {
        var user = userService.createUser(userCreateDTO);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @GetMapping(value = "/users/{id}")
    protected ResponseEntity<?> getUser(@PathVariable Long id) {
        var user = userService.getUser(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PatchMapping(value = "/users/{id}")
    protected ResponseEntity<?> updateUser(@PathVariable Long id, @RequestBody UserCreateDTO userUpdateDTO) {
        var user = userService.updateUser(id, userUpdateDTO);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}
