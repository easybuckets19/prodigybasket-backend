package com.easyBucketSoft.prodigy.api.controller;

import com.easyBucketSoft.prodigy.api.data.payload.referee.RefereeCreateDTO;
import com.easyBucketSoft.prodigy.core.service.contract.RefereeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RefereeController {
    private final RefereeService refereeService;

    public RefereeController(RefereeService refereeService) {
        this.refereeService = refereeService;
    }

    @GetMapping(value = "/referees")
    protected ResponseEntity<?> getReferees() {
        var referees = refereeService.getReferees();
        return new ResponseEntity<>(referees, HttpStatus.OK);
    }

    @PostMapping(value = "/referees")
    protected ResponseEntity<?> createReferee(@RequestBody RefereeCreateDTO refereeCreateDTO) {
        var referee = refereeService.createReferee(refereeCreateDTO);
        return new ResponseEntity<>(referee, HttpStatus.CREATED);
    }

    @GetMapping(value = "/referees/{id}")
    protected ResponseEntity<?> getReferee(@PathVariable Long id) {
        var referee = refereeService.getReferee(id);
        return new ResponseEntity<>(referee, HttpStatus.OK);
    }

    @PatchMapping(value = "/referees/{id}")
    protected ResponseEntity<?> updateReferee(@PathVariable Long id, @RequestBody RefereeCreateDTO refereeUpdateDTO) {
        var referee = refereeService.updateReferee(id, refereeUpdateDTO);
        return new ResponseEntity<>(referee, HttpStatus.OK);
    }

}
