package com.easyBucketSoft.prodigy.api.controller;

import com.easyBucketSoft.prodigy.api.data.payload.employment.EmploymentCreateDTO;

import com.easyBucketSoft.prodigy.core.service.contract.EmploymentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmploymentController {
    private final EmploymentService employmentService;

    public EmploymentController(EmploymentService employmentService) {
        this.employmentService = employmentService;
    }

    @GetMapping(value = "/employments")
    protected ResponseEntity<?> getEmployments() {
        var employments = employmentService.getEmployments();
        return new ResponseEntity<>(employments, HttpStatus.OK);
    }

    @PostMapping(value = "/employments")
    protected ResponseEntity<?> createEmployment(@RequestBody EmploymentCreateDTO employmentCreateDTO) {
        var employment = employmentService.createEmployment(employmentCreateDTO);
        return new ResponseEntity<>(employment, HttpStatus.CREATED);
    }

    @GetMapping(value = "/employments/{id}")
    protected ResponseEntity<?> getEmployment(@PathVariable Long id) {
        var employment = employmentService.getEmployment(id);
        return new ResponseEntity<>(employment, HttpStatus.OK);
    }

    @PatchMapping(value = "/employments/{id}")
    protected ResponseEntity<?> updateEmployment(@PathVariable Long id, @RequestBody EmploymentCreateDTO employmentUpdateDTO) {
        var employment = employmentService.updateEmployment(id, employmentUpdateDTO);
        return new ResponseEntity<>(employment, HttpStatus.OK);
    }

    @GetMapping(value = "/coaches/{id}/employments")
    protected ResponseEntity<?> getEmploymentsByCoach(@PathVariable Long id) {
        var employments = employmentService.getEmploymentsByCoach(id);
        return new ResponseEntity<>(employments, HttpStatus.OK);
    }

    @GetMapping(value = "/teams/{id}/employments")
    protected ResponseEntity<?> getEmploymentsByTeam(@PathVariable Long id) {
        var employments = employmentService.getEmploymentsByTeam(id);
        return new ResponseEntity<>(employments, HttpStatus.OK);
    }

}
