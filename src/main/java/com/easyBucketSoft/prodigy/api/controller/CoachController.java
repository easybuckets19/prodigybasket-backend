package com.easyBucketSoft.prodigy.api.controller;

import com.easyBucketSoft.prodigy.api.data.payload.coach.CoachCreateDTO;
import com.easyBucketSoft.prodigy.core.service.contract.CoachService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CoachController {
    private final CoachService coachService;

    public CoachController(CoachService coachService) {
        this.coachService = coachService;
    }

    @GetMapping(value = "/coaches")
    protected ResponseEntity<?> getCoaches() {
        var coaches = coachService.getCoaches();
        return new ResponseEntity<>(coaches, HttpStatus.OK);
    }

    @PostMapping(value = "/coaches")
    protected ResponseEntity<?> createCoach(@RequestBody CoachCreateDTO coachCreateDTO) {
        var coach = coachService.createCoach(coachCreateDTO);
        return new ResponseEntity<>(coach, HttpStatus.CREATED);
    }

    @GetMapping(value = "/coaches/{id}")
    protected ResponseEntity<?> getCoach(@PathVariable Long id) {
        var coach = coachService.getCoach(id);
        return new ResponseEntity<>(coach, HttpStatus.OK);
    }

    @PatchMapping(value = "/coaches/{id}")
    protected ResponseEntity<?> updateCoach(@PathVariable Long id, @RequestBody CoachCreateDTO coachUpdateDTO) {
        var coach = coachService.updateCoach(id, coachUpdateDTO);
        return new ResponseEntity<>(coach, HttpStatus.OK);
    }

}
