package com.easyBucketSoft.prodigy.api.controller;

import com.easyBucketSoft.prodigy.api.data.payload.contract.ContractCreateDTO;
import com.easyBucketSoft.prodigy.core.service.contract.ContractService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ContractController {
    private final ContractService contractService;

    public ContractController(ContractService contractService) {
        this.contractService = contractService;
    }

    @GetMapping(value = "/contracts")
    protected ResponseEntity<?> getContracts() {
        var contracts = contractService.getContracts();
        return new ResponseEntity<>(contracts, HttpStatus.OK);
    }

    @PostMapping(value = "/contracts")
    protected ResponseEntity<?> createContract(@RequestBody ContractCreateDTO contractCreateDTO) {
        var contract = contractService.createContract(contractCreateDTO);
        return new ResponseEntity<>(contract, HttpStatus.CREATED);
    }

    @GetMapping(value = "/contracts/{id}")
    protected ResponseEntity<?> getContract(@PathVariable Long id) {
        var contract = contractService.getContract(id);
        return new ResponseEntity<>(contract, HttpStatus.OK);
    }

    @PatchMapping(value = "/contracts/{id}")
    protected ResponseEntity<?> updateContract(@PathVariable Long id, @RequestBody ContractCreateDTO contractUpdateDTO) {
        var contract = contractService.updateContract(id, contractUpdateDTO);
        return new ResponseEntity<>(contract, HttpStatus.OK);
    }

    @GetMapping(value = "/players/{id}/contracts")
    protected ResponseEntity<?> getContractsByPlayer(@PathVariable Long id) {
        var contracts = contractService.getContractsByPlayer(id);
        return new ResponseEntity<>(contracts, HttpStatus.OK);
    }

    @GetMapping(value = "/teams/{id}/contracts")
    protected ResponseEntity<?> getContractsByTeam(@PathVariable Long id) {
        var contracts = contractService.getContractsByTeam(id);
        return new ResponseEntity<>(contracts, HttpStatus.OK);
    }

}
