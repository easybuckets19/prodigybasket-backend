package com.easyBucketSoft.prodigy.api.controller;

import com.easyBucketSoft.prodigy.api.data.payload.player.PlayerCreateDTO;
import com.easyBucketSoft.prodigy.core.service.contract.PlayerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PlayerController {
    private final PlayerService playerService;

    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @GetMapping(value = "/players")
    protected ResponseEntity<?> getPlayers() {
        var players = playerService.getPlayers();
        return new ResponseEntity<>(players, HttpStatus.OK);
    }

    @PostMapping(value = "/players")
    protected ResponseEntity<?> createPlayer(@RequestBody PlayerCreateDTO playerCreateDTO) {
        var player = playerService.createPlayer(playerCreateDTO);
        return new ResponseEntity<>(player, HttpStatus.CREATED);
    }

    @GetMapping(value = "/players/{id}")
    protected ResponseEntity<?> getPlayer(@PathVariable Long id) {
        var player = playerService.getPlayer(id);
        return new ResponseEntity<>(player, HttpStatus.OK);
    }

    @PatchMapping(value = "/players/{id}")
    protected ResponseEntity<?> updatePlayer(@PathVariable Long id, @RequestBody PlayerCreateDTO playerUpdateDTO) {
        var player = playerService.updatePlayer(id, playerUpdateDTO);
        return new ResponseEntity<>(player, HttpStatus.OK);
    }

}
