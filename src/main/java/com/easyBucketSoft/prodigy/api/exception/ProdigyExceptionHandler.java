package com.easyBucketSoft.prodigy.api.exception;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.Collections;

@RestControllerAdvice
public class ProdigyExceptionHandler {
    
    @ExceptionHandler(ProdigyException.class)
    protected ResponseEntity<ProdigyError> handleBusinessLogicExceptions(ProdigyException exception, HttpServletRequest request) {
        ProdigyError response = new ProdigyError()
            .setUrl(request.getRequestURL().toString())
            .setPath(request.getRequestURI())
            .setMessage(exception.getMessage())
            .setDetails(Collections.emptyList())
            .setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(response, exception.getStatus());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<ProdigyError> handleBusinessLogicExceptions(ConstraintViolationException exception, HttpServletRequest request) {
        ProdigyError response = new ProdigyError()
            .setUrl(request.getRequestURL().toString())
            .setPath(request.getRequestURI())
            .setMessage(exception.getMessage())
            .setDetails(Collections.emptyList())
            .setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


}