package com.easyBucketSoft.prodigy.api.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProdigyErrorDetails {
    private String field;
    private String description;
}
