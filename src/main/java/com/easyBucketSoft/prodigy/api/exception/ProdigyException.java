package com.easyBucketSoft.prodigy.api.exception;

import org.springframework.http.HttpStatus;

public class ProdigyException extends RuntimeException {
    private final HttpStatus status;

    public ProdigyException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

}
