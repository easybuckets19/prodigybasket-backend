package com.easyBucketSoft.prodigy.api.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProdigyError {
    private String url;
    private String path;
    private String message;
    private LocalDateTime timestamp;
    private List<ProdigyErrorDetails> details;
}
