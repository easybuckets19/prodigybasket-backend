package com.easyBucketSoft.prodigy.database.listener;


import com.easyBucketSoft.prodigy.database.model.User;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PasswordEncodingListener {
    private final PasswordEncoder encoder;
    private static final String HASH_PREFIX = "$2a$";

    @Lazy
    public PasswordEncodingListener(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @PreUpdate
    public void onUpdate(User user) {
        String rawPassword = user.getPassword();
        if (!rawPassword.startsWith(HASH_PREFIX)) {
            user.setPassword(encoder.encode(rawPassword));
        }

    }

    @PrePersist
    public void onPersist(User user) {
        String rawPassword = user.getPassword();
        if (!rawPassword.startsWith(HASH_PREFIX)) {
            user.setPassword(encoder.encode(rawPassword));
        }
    }

}
