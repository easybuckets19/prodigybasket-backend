package com.easyBucketSoft.prodigy.database.model;

import jakarta.persistence.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity(name = "Contracts")
@EntityListeners(value = AuditingEntityListener.class)
public class Contract {
    private Long id;
    private LocalDate start;
    private LocalDate finish;
    private Boolean active;
    private Short jersey;
    private Position position;
    private Team team;
    private Player player;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public Contract() {}

    public Contract(LocalDate start, Short jersey, Position position, Team team, Player player) {
        this.start = start;
        this.jersey = jersey;
        this.position = position;
        this.team = team;
        this.player = player;
        this.activate();
    }

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStart() {
        return start;
    }
    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getFinish() {
        return finish;
    }
    public void setFinish(LocalDate finish) {
        this.finish = finish;
    }

    public Boolean isActive() {
        return active;
    }
    public void setActive(Boolean active) {
        this.active = active;
    }

    public Short getJersey() {
        return jersey;
    }
    public void setJersey(Short jersey) {
        this.jersey = jersey;
    }

    @Enumerated(value = EnumType.STRING)
    public Position getPosition() {
        return position;
    }
    public void setPosition(Position position) {
        this.position = position;
    }

    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    public Team getTeam() {
        return team;
    }
    public void setTeam(Team team) {
        this.team = team;
    }

    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }

    @CreatedDate
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @LastModifiedDate
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public enum Position   { PG, SG, SF, PF, C }

    @Transient
    public void activate() {
        this.active = true;
    }

    @Transient
    public void deactivate() {
        this.active = false;
    }

    @PrePersist
    protected void onPersist() {
        this.activate();
    }
}
