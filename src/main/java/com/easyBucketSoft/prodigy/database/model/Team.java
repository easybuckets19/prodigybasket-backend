package com.easyBucketSoft.prodigy.database.model;

import jakarta.persistence.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Entity(name = "Teams")
@EntityListeners(value = AuditingEntityListener.class)
public class Team {
    private Long id;
    private String name;
    private User owner;
    private Court court;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public Team() {}

    public Team(String name, User owner, Court court) {
        this.name = name;
        this.owner = owner;
        this.court = court;
    }

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    public User getOwner() {
        return owner;
    }
    public void setOwner(User owner) {
        this.owner = owner;
    }

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    public Court getCourt() {
        return court;
    }
    public void setCourt(Court court) {
        this.court = court;
    }

    @CreatedDate
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @LastModifiedDate
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

}
