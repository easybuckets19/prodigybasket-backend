package com.easyBucketSoft.prodigy.database.embeddable;

import jakarta.persistence.Embeddable;

import java.util.Objects;

@Embeddable
public class Address {
    private String country;
    private String city;
    private String code;
    private String street;
    private Double lat;
    private Double lng;

    public Address() {}

    public Address(String country, String city, String code, String street, Double lat, Double lng) {
        this.country = country;
        this.city = city;
        this.code = code;
        this.street = street;
        this.lat = lat;
        this.lng = lng;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }

    public Double getLat() {
        return lat;
    }
    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }
    public void setLng(Double lng) {
        this.lng = lng;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(lat, address.lat) && Objects.equals(lng, address.lng);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lat, lng);
    }

}
