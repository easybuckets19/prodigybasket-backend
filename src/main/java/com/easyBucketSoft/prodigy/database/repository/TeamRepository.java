package com.easyBucketSoft.prodigy.database.repository;

import com.easyBucketSoft.prodigy.database.embeddable.Address;
import com.easyBucketSoft.prodigy.database.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {

    Boolean existsByName(String name);

    Optional<Team> findByName(String name);

    Boolean existsByCourtAddress(Address address);

    Optional<Team>  findByCourtAddress(Address address);

}