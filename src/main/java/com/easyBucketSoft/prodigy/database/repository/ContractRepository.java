package com.easyBucketSoft.prodigy.database.repository;

import com.easyBucketSoft.prodigy.database.model.Contract;
import com.easyBucketSoft.prodigy.database.model.Player;
import com.easyBucketSoft.prodigy.database.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContractRepository extends JpaRepository<Contract, Long> {

    List<Contract> findByPlayer(Player player);

    List<Contract> findByTeam(Team team);

    @Query(value = "select c from Contracts c where c.team=:team and c.jersey=:jersey and c.active = true")
    Optional<Contract> findByTeamAndJersey(Team team, Short jersey);

}