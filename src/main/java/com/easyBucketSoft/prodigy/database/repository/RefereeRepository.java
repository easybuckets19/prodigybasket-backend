package com.easyBucketSoft.prodigy.database.repository;

import com.easyBucketSoft.prodigy.database.model.Referee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RefereeRepository extends JpaRepository<Referee, Long> {}