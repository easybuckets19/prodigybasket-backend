package com.easyBucketSoft.prodigy.database.repository;

import com.easyBucketSoft.prodigy.database.model.Coach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoachRepository extends JpaRepository<Coach, Long> {}