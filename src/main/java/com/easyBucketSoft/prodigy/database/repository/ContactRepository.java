package com.easyBucketSoft.prodigy.database.repository;

import com.easyBucketSoft.prodigy.database.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

    Optional<Contact> findByEmail(String email);

    Boolean existsByEmail(String email);

}
