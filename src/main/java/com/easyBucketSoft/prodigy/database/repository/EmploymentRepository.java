package com.easyBucketSoft.prodigy.database.repository;

import com.easyBucketSoft.prodigy.database.model.Coach;
import com.easyBucketSoft.prodigy.database.model.Employment;
import com.easyBucketSoft.prodigy.database.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmploymentRepository extends JpaRepository<Employment, Long> {

    List<Employment> findByCoach(Coach coach);

    List<Employment> findByTeam(Team team);

    @Query(value = "select e from Employments e where e.team=:team and e.position=:position and e.active = true")
    Optional<Employment> findByTeamAndPosition(Team team, Employment.Position position);

}