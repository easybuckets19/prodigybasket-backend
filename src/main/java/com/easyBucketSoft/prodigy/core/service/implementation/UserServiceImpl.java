package com.easyBucketSoft.prodigy.core.service.implementation;

import com.easyBucketSoft.prodigy.api.data.mapper.UserMapper;
import com.easyBucketSoft.prodigy.api.data.payload.user.UserCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.user.UserDTO;
import com.easyBucketSoft.prodigy.api.exception.ProdigyException;
import com.easyBucketSoft.prodigy.core.service.contract.UserService;
import com.easyBucketSoft.prodigy.database.model.User;
import com.easyBucketSoft.prodigy.database.repository.ContactRepository;
import com.easyBucketSoft.prodigy.database.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final ContactRepository contactRepository;

    public UserServiceImpl(UserMapper userMapper, UserRepository userRepository, ContactRepository contactRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.contactRepository = contactRepository;
    }


    public List<UserDTO> getUsers() {
        List<User> users = userRepository.findAll();
        return userMapper.mapUsers(users);
    }

    public UserDTO getUser(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given user was not found"
            );
        }
        return userMapper.mapUser(user.get());
    }

    public UserDTO createUser(UserCreateDTO userCreateDTO) {
        Boolean usernameAlreadyUsed   = userRepository.existsByUsername(userCreateDTO.username());
        Boolean emailAlreadyUsed      = contactRepository.existsByEmail(userCreateDTO.contact().email());
        if (usernameAlreadyUsed) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given username is already used"
            );
        }

        if (emailAlreadyUsed) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given email is already used"
            );
        }

        User user = userMapper.createUser(userCreateDTO);
        return userMapper.mapUser(userRepository.save(user));
    }


    public UserDTO updateUser(Long id, UserCreateDTO userUpdateDTO) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given user was not found"
            );
        }
        userMapper.updateUser(user.get(), userUpdateDTO);
        return userMapper.mapUser(userRepository.save(user.get()));
    }

}
