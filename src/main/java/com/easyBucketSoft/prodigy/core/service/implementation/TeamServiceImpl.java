package com.easyBucketSoft.prodigy.core.service.implementation;

import com.easyBucketSoft.prodigy.api.data.mapper.TeamMapper;
import com.easyBucketSoft.prodigy.api.data.payload.team.TeamCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.team.TeamDTO;
import com.easyBucketSoft.prodigy.api.exception.ProdigyException;
import com.easyBucketSoft.prodigy.core.service.contract.TeamService;
import com.easyBucketSoft.prodigy.database.model.Team;
import com.easyBucketSoft.prodigy.database.model.User;
import com.easyBucketSoft.prodigy.database.repository.TeamRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeamServiceImpl implements TeamService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TeamService.class);
    private final TeamRepository teamRepository;
    private final TeamMapper teamMapper;

    public TeamServiceImpl(TeamRepository teamRepository, TeamMapper teamMapper) {
        this.teamRepository = teamRepository;
        this.teamMapper = teamMapper;
    }

    @Override
    public List<TeamDTO> getTeams() {
        List<Team> teams = teamRepository.findAll();
        return teamMapper.mapTeams(teams);
    }

    @Override
    public TeamDTO getTeam(Long id) {
        Optional<Team> team = teamRepository.findById(id);
        if (team.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given team was not found"
            );
        }
        return teamMapper.mapTeam(team.get());
    }

    @Override
    public TeamDTO createTeam(TeamCreateDTO teamCreateDTO) {
        Boolean nameAlreadyUsed  = teamRepository.existsByName(teamCreateDTO.name());
        Boolean courtAlreadyUsed = teamRepository.existsByCourtAddress(teamCreateDTO.court().address());

        if (nameAlreadyUsed) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given username is already used"
            );
        }

        if (courtAlreadyUsed) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given location is already used"
            );
        }

        Team team = teamMapper.createTeam(teamCreateDTO);
        User owner = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        team.setOwner(owner);
        return teamMapper.mapTeam(teamRepository.save(team));
    }

    @Override
    public TeamDTO updateTeam(Long id, TeamCreateDTO teamCreateDTO) {
        Optional<Team> team = teamRepository.findById(id);
        if (team.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given team was not found"
            );
        }
        teamMapper.updateTeam(team.get(), teamCreateDTO);
        return teamMapper.mapTeam(teamRepository.save(team.get()));
    }

}
