package com.easyBucketSoft.prodigy.core.service.implementation;

import com.easyBucketSoft.prodigy.api.data.mapper.CoachMapper;
import com.easyBucketSoft.prodigy.api.data.payload.coach.CoachCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.coach.CoachDTO;
import com.easyBucketSoft.prodigy.api.exception.ProdigyException;
import com.easyBucketSoft.prodigy.core.service.contract.CoachService;
import com.easyBucketSoft.prodigy.database.model.Coach;
import com.easyBucketSoft.prodigy.database.repository.CoachRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CoachServiceImpl implements CoachService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CoachService.class);
    private final CoachRepository coachRepository;
    private final CoachMapper coachMapper;

    public CoachServiceImpl(CoachRepository coachRepository, CoachMapper coachMapper) {
        this.coachRepository = coachRepository;
        this.coachMapper = coachMapper;
    }

    @Override
    public List<CoachDTO> getCoaches() {
        List<Coach> coaches = coachRepository.findAll();
        return coachMapper.mapCoaches(coaches);
    }

    @Override
    public CoachDTO getCoach(Long id) {
        Optional<Coach> coach = coachRepository.findById(id);
        if (coach.isEmpty()) {
            throw new ProdigyException(
              HttpStatus.NOT_FOUND,
              "The given coach was not found"
            );
        }
        return coachMapper.mapCoach(coach.get());
    }

    @Override
    public CoachDTO createCoach(CoachCreateDTO coachCreateDTO) {
        Coach coach = coachMapper.createCoach(coachCreateDTO);
        return coachMapper.mapCoach(coachRepository.save(coach));
    }

    @Override
    public CoachDTO updateCoach(Long id, CoachCreateDTO coachUpdateDTO) {
        Optional<Coach> coach = coachRepository.findById(id);
        if (coach.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given coach was not found"
            );
        }
        coachMapper.updateCoach(coach.get(), coachUpdateDTO);
        return coachMapper.mapCoach(coachRepository.save(coach.get()));
    }

}
