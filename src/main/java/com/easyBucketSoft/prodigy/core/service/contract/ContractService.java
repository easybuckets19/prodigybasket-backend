package com.easyBucketSoft.prodigy.core.service.contract;

import com.easyBucketSoft.prodigy.api.data.payload.contract.ContractCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.contract.ContractDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface ContractService {

    List<ContractDTO> getContracts();

    ContractDTO getContract(@NotNull Long id);

    ContractDTO createContract(@Valid ContractCreateDTO contractCreateDTO);

    ContractDTO updateContract(@NotNull Long id, @Valid ContractCreateDTO contractUpdateDTO);

    List<ContractDTO> getContractsByPlayer(@NotNull Long playerId);

    List<ContractDTO> getContractsByTeam(@NotNull Long teamId);

}