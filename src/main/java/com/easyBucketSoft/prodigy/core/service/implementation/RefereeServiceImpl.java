package com.easyBucketSoft.prodigy.core.service.implementation;

import com.easyBucketSoft.prodigy.api.data.mapper.RefereeMapper;
import com.easyBucketSoft.prodigy.api.data.payload.referee.RefereeCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.referee.RefereeDTO;
import com.easyBucketSoft.prodigy.api.exception.ProdigyException;
import com.easyBucketSoft.prodigy.core.service.contract.RefereeService;
import com.easyBucketSoft.prodigy.database.model.Referee;
import com.easyBucketSoft.prodigy.database.repository.RefereeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RefereeServiceImpl implements RefereeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RefereeService.class);
    private final RefereeRepository refereeRepository;
    private final RefereeMapper refereeMapper;

    public RefereeServiceImpl(RefereeRepository refereeRepository, RefereeMapper refereeMapper) {
        this.refereeRepository = refereeRepository;
        this.refereeMapper = refereeMapper;
    }

    @Override
    public List<RefereeDTO> getReferees() {
        List<Referee> referees = refereeRepository.findAll();
        return refereeMapper.mapReferees(referees);
    }

    @Override
    public RefereeDTO getReferee(Long id) {
        Optional<Referee> referee = refereeRepository.findById(id);
        if (referee.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given referee was not found"
            );
        }
        return refereeMapper.mapReferee(referee.get());
    }

    @Override
    public RefereeDTO createReferee(RefereeCreateDTO refereeCreateDTO) {
        Referee referee = refereeMapper.createReferee(refereeCreateDTO);
        return refereeMapper.mapReferee(refereeRepository.save(referee));
    }

    @Override
    public RefereeDTO updateReferee(Long id, RefereeCreateDTO refereeUpdateDTO) {
        Optional<Referee> referee = refereeRepository.findById(id);
        if (referee.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given referee was not found"
            );
        }
        refereeMapper.updateReferee(referee.get(), refereeUpdateDTO);
        return refereeMapper.mapReferee(refereeRepository.save(referee.get()));
    }

}
