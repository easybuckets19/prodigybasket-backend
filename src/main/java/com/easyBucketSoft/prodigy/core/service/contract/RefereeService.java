package com.easyBucketSoft.prodigy.core.service.contract;

import com.easyBucketSoft.prodigy.api.data.payload.referee.RefereeCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.referee.RefereeDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface RefereeService {

    List<RefereeDTO> getReferees();

    RefereeDTO getReferee(@NotNull Long id);

    RefereeDTO createReferee(@Valid RefereeCreateDTO refereeCreateDTO);

    RefereeDTO updateReferee(@NotNull Long id, @Valid RefereeCreateDTO refereeUpdateDTO);

}
