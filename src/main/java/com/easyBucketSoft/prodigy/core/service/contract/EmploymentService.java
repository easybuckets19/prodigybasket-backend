package com.easyBucketSoft.prodigy.core.service.contract;

import com.easyBucketSoft.prodigy.api.data.payload.employment.EmploymentCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.employment.EmploymentDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface EmploymentService {

    List<EmploymentDTO> getEmployments();

    EmploymentDTO getEmployment(@NotNull Long id);

    EmploymentDTO createEmployment(@Valid EmploymentCreateDTO employmentCreateDTO);

    EmploymentDTO updateEmployment(@NotNull Long id, @Valid EmploymentCreateDTO employmentUpdateDTO);

    List<EmploymentDTO> getEmploymentsByCoach(@NotNull Long coachId);

    List<EmploymentDTO> getEmploymentsByTeam(@NotNull Long teamId);

}