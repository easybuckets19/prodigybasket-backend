package com.easyBucketSoft.prodigy.core.service.implementation;

import com.easyBucketSoft.prodigy.api.data.mapper.EmploymentMapper;
import com.easyBucketSoft.prodigy.api.data.payload.employment.EmploymentCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.employment.EmploymentDTO;
import com.easyBucketSoft.prodigy.api.exception.ProdigyException;
import com.easyBucketSoft.prodigy.core.service.contract.EmploymentService;
import com.easyBucketSoft.prodigy.database.model.Coach;
import com.easyBucketSoft.prodigy.database.model.Contract;
import com.easyBucketSoft.prodigy.database.model.Employment;
import com.easyBucketSoft.prodigy.database.model.Team;
import com.easyBucketSoft.prodigy.database.repository.CoachRepository;
import com.easyBucketSoft.prodigy.database.repository.EmploymentRepository;
import com.easyBucketSoft.prodigy.database.repository.TeamRepository;
import org.hibernate.cfg.NotYetImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmploymentServiceImpl implements EmploymentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmploymentService.class);
    private final EmploymentMapper employmentMapper;
    private final EmploymentRepository employmentRepository;
    private final CoachRepository coachRepository;
    private final TeamRepository teamRepository;

    private static final Integer COACH_ACTIVE_EMPLOYMENT_LIMIT  = 1;
    private static final Integer TEAM_ACTIVE_EMPLOYMENT_LIMIT   = 3;

    public EmploymentServiceImpl(EmploymentMapper employmentMapper, EmploymentRepository employmentRepository,
                                 CoachRepository coachRepository, TeamRepository teamRepository) {
        this.employmentMapper = employmentMapper;
        this.employmentRepository = employmentRepository;
        this.coachRepository = coachRepository;
        this.teamRepository = teamRepository;
    }

    @Override
    public List<EmploymentDTO> getEmployments() {
        List<Employment> employments = employmentRepository.findAll();
        return employmentMapper.mapEmployments(employments);
    }

    @Override
    public EmploymentDTO getEmployment(Long id) {
        Optional<Employment> employment = employmentRepository.findById(id);
        if (employment.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given employment was not found"
            );
        }
        return employmentMapper.mapEmployment(employment.get());
    }

    @Override
    public EmploymentDTO createEmployment(EmploymentCreateDTO employmentCreateDTO) {
        Coach coach = coachRepository.findById(employmentCreateDTO.coach()).orElseThrow(() ->
            new ProdigyException(HttpStatus.CONFLICT, "The given coach was not found")
        );
        Team team = teamRepository.findById(employmentCreateDTO.team()).orElseThrow(() ->
            new ProdigyException(HttpStatus.CONFLICT, "The given team was not found")
        );

        List<Employment> coachEmployments = employmentRepository.findByCoach(coach);
        var coachActiveEmployments = coachEmployments.stream().filter(Employment::isActive).toList();
        if (coachActiveEmployments.size() >= COACH_ACTIVE_EMPLOYMENT_LIMIT) {
            throw new ProdigyException(
                HttpStatus.CONFLICT,
                "The given coach is already associated with another team"
            );
        }

        List<Employment> teamEmployments = employmentRepository.findByTeam(team);
        var teamActiveEmployments = teamEmployments.stream().filter(Employment::isActive).toList();
        if (teamActiveEmployments.size() >= TEAM_ACTIVE_EMPLOYMENT_LIMIT) {
            throw new ProdigyException(
                HttpStatus.CONFLICT,
                "The given team already reached the maximum employment limit."
            );
        }

        Optional<Employment> employmentWithPosition = employmentRepository.findByTeamAndPosition(team, employmentCreateDTO.position());
        if (employmentWithPosition.isPresent() && employmentWithPosition.get().isActive()) {
            throw new ProdigyException(
                HttpStatus.CONFLICT,
                "The given position is already filled by another coach "
            );
        }
        Employment employment = employmentMapper.createEmployment(employmentCreateDTO);
        return employmentMapper.mapEmployment(employmentRepository.save(employment));
    }

    @Override
    public EmploymentDTO updateEmployment(Long id, EmploymentCreateDTO employmentUpdateDTO) {
        Optional<Employment> employment = employmentRepository.findById(id);
        if (employment.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given employment was not found"
            );
        }
        employmentMapper.updateEmployment(employment.get(), employmentUpdateDTO);
        return employmentMapper.mapEmployment(employmentRepository.save(employment.get()));
    }

    @Override
    public List<EmploymentDTO> getEmploymentsByCoach(Long coachId) {
        Optional<Coach> coach = coachRepository.findById(coachId);
        if (coach.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given coach was not found"
            );
        }
        List<Employment> employments = employmentRepository.findByCoach(coach.get());
        return employmentMapper.mapEmployments(employments);
    }

    @Override
    public List<EmploymentDTO> getEmploymentsByTeam(Long teamId) {
        Optional<Team> team = teamRepository.findById(teamId);
        if (team.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given team was not found"
            );
        }
        List<Employment> employments = employmentRepository.findByTeam(team.get());
        return employmentMapper.mapEmployments(employments);
    }

}

