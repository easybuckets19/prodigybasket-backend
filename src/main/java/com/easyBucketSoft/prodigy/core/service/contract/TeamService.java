package com.easyBucketSoft.prodigy.core.service.contract;

import com.easyBucketSoft.prodigy.api.data.payload.team.TeamCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.team.TeamDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface TeamService {

    List<TeamDTO> getTeams();

    TeamDTO getTeam(@NotNull Long id);

    TeamDTO createTeam(@Valid TeamCreateDTO teamCreateDTO);

    TeamDTO updateTeam(@NotNull Long id, @Valid TeamCreateDTO teamUpdateDTO);

}
