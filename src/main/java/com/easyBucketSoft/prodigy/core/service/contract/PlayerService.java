package com.easyBucketSoft.prodigy.core.service.contract;

import com.easyBucketSoft.prodigy.api.data.payload.player.PlayerCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.player.PlayerDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface PlayerService {

    List<PlayerDTO> getPlayers();

    PlayerDTO getPlayer(@NotNull Long id);

    PlayerDTO createPlayer(@Valid PlayerCreateDTO playerCreateDTO);

    PlayerDTO updatePlayer(@NotNull Long id, @Valid PlayerCreateDTO playerUpdateDTO);

}
