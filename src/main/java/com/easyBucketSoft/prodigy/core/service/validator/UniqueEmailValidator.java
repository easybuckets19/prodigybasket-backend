package com.easyBucketSoft.prodigy.core.service.validator;


import com.easyBucketSoft.prodigy.database.repository.ContactRepository;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.stereotype.Component;

@Component
public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {
    private final ContactRepository contactRepository;

    public UniqueEmailValidator(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !contactRepository.existsByEmail(value);
    }

}
