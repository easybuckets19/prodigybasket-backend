package com.easyBucketSoft.prodigy.core.service.contract;

import com.easyBucketSoft.prodigy.api.data.payload.coach.CoachCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.coach.CoachDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface CoachService {

    List<CoachDTO> getCoaches();

    CoachDTO getCoach(@NotNull Long id);

    CoachDTO createCoach(@Valid CoachCreateDTO coachCreateDTO);

    CoachDTO updateCoach(@NotNull Long id, @Valid CoachCreateDTO coachUpdateDTO);

}
