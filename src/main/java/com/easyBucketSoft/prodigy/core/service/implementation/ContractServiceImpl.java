package com.easyBucketSoft.prodigy.core.service.implementation;

import com.easyBucketSoft.prodigy.api.data.mapper.ContractMapper;
import com.easyBucketSoft.prodigy.api.data.payload.contract.ContractCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.contract.ContractDTO;
import com.easyBucketSoft.prodigy.api.exception.ProdigyException;
import com.easyBucketSoft.prodigy.core.service.contract.ContractService;
import com.easyBucketSoft.prodigy.database.model.Contract;
import com.easyBucketSoft.prodigy.database.model.Player;
import com.easyBucketSoft.prodigy.database.model.Team;
import com.easyBucketSoft.prodigy.database.repository.ContractRepository;
import com.easyBucketSoft.prodigy.database.repository.PlayerRepository;
import com.easyBucketSoft.prodigy.database.repository.TeamRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContractServiceImpl implements ContractService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContractService.class);
    private final ContractMapper contractMapper;
    private final ContractRepository contractRepository;
    private final PlayerRepository playerRepository;
    private final TeamRepository teamRepository;

    private static final Integer PLAYER_ACTIVE_CONTRACT_LIMIT   = 1;
    private static final Integer TEAM_ACTIVE_CONTRACT_LIMIT     = 12;

    public ContractServiceImpl(ContractMapper contractMapper, ContractRepository contractRepository,
                               PlayerRepository playerRepository, TeamRepository teamRepository) {
        this.contractMapper = contractMapper;
        this.contractRepository = contractRepository;
        this.playerRepository = playerRepository;
        this.teamRepository = teamRepository;
    }

    @Override
    public List<ContractDTO> getContracts() {
        List<Contract> contracts = contractRepository.findAll();
        return contractMapper.mapContracts(contracts);
    }

    @Override
    public ContractDTO getContract(Long id) {
        Optional<Contract> contract = contractRepository.findById(id);
        if (contract.isEmpty()) {
           throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given contract was not found"
           );
        }
        return contractMapper.mapContract(contract.get());
    }

    @Override
    public ContractDTO createContract(ContractCreateDTO contractCreateDTO) {
        Player player = playerRepository.findById(contractCreateDTO.player()).orElseThrow(() ->
            new ProdigyException(HttpStatus.CONFLICT, "The given player was not found")
        );
        Team team = teamRepository.findById(contractCreateDTO.team()).orElseThrow(() ->
            new ProdigyException(HttpStatus.CONFLICT, "The given team was not found")
        );

        List<Contract> playerContracts = contractRepository.findByPlayer(player);
        var playerActiveContracts = playerContracts.stream().filter(Contract::isActive).toList();
        if (playerActiveContracts.size() >= PLAYER_ACTIVE_CONTRACT_LIMIT) {
            throw new ProdigyException(
                HttpStatus.CONFLICT,
                "The given player is already associated with another team"
            );
        }

        List<Contract> teamContracts = contractRepository.findByTeam(team);
        var teamActiveContracts = teamContracts.stream().filter(Contract::isActive).toList();
        if (teamActiveContracts.size() >= TEAM_ACTIVE_CONTRACT_LIMIT) {
            throw new ProdigyException(
                HttpStatus.CONFLICT,
                "The given team already reached the maximum contract limit."
            );
        }

        Optional<Contract> contractWithJersey = contractRepository.findByTeamAndJersey(team, contractCreateDTO.jersey());
        if (contractWithJersey.isPresent()) {
            throw new ProdigyException(
                HttpStatus.CONFLICT,
                "The given jersey is already taken by another player"
            );
        }
        Contract contract = contractMapper.createContract(contractCreateDTO);
        return contractMapper.mapContract(contractRepository.save(contract));

    }

    @Override
    public ContractDTO updateContract(Long id, ContractCreateDTO contractUpdateDTO) {
        Optional<Contract> contract = contractRepository.findById(id);
        if (contract.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given contract was not found"
            );
        }
        contractMapper.updateContract(contract.get(), contractUpdateDTO);
        return contractMapper.mapContract(contractRepository.save(contract.get()));
    }

    @Override
    public List<ContractDTO> getContractsByPlayer(Long playerId) {
        Optional<Player> player = playerRepository.findById(playerId);
        if (player.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given player was not found"
            );
        }
        List<Contract> contracts = contractRepository.findByPlayer(player.get());
        return contractMapper.mapContracts(contracts);
    }

    @Override
    public List<ContractDTO> getContractsByTeam(Long teamId) {
        Optional<Team> team = teamRepository.findById(teamId);
        if (team.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given team was not found"
            );
        }
        List<Contract> contracts = contractRepository.findByTeam(team.get());
        return contractMapper.mapContracts(contracts);
    }

}
