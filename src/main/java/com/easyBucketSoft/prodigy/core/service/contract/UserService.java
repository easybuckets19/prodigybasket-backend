package com.easyBucketSoft.prodigy.core.service.contract;

import com.easyBucketSoft.prodigy.api.data.payload.user.UserCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.user.UserDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface UserService {

    List<UserDTO> getUsers();

    UserDTO getUser(@NotNull Long id);

    UserDTO createUser(@Valid UserCreateDTO userCreateDTO);

    UserDTO updateUser(@NotNull Long id, @Valid UserCreateDTO userUpdateDTO);

}
