package com.easyBucketSoft.prodigy.core.service.implementation;

import com.easyBucketSoft.prodigy.api.data.mapper.PlayerMapper;
import com.easyBucketSoft.prodigy.api.data.payload.player.PlayerCreateDTO;
import com.easyBucketSoft.prodigy.api.data.payload.player.PlayerDTO;
import com.easyBucketSoft.prodigy.api.exception.ProdigyException;
import com.easyBucketSoft.prodigy.core.service.contract.PlayerService;
import com.easyBucketSoft.prodigy.database.model.Player;
import com.easyBucketSoft.prodigy.database.repository.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerServiceImpl implements PlayerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerService.class);
    private final PlayerRepository playerRepository;
    private final PlayerMapper playerMapper;

    public PlayerServiceImpl(PlayerRepository playerRepository, PlayerMapper playerMapper) {
        this.playerRepository = playerRepository;
        this.playerMapper = playerMapper;
    }

    @Override
    public List<PlayerDTO> getPlayers() {
        List<Player> players = playerRepository.findAll();
        return playerMapper.mapPlayers(players);
    }

    @Override
    public PlayerDTO getPlayer(Long id) {
        Optional<Player> player = playerRepository.findById(id);
        if (player.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given player was not found"
            );
        }
        return playerMapper.mapPlayer(player.get());
    }

    @Override
    public PlayerDTO createPlayer(PlayerCreateDTO playerCreateDTO) {
        Player player = playerMapper.createPlayer(playerCreateDTO);
        return playerMapper.mapPlayer(playerRepository.save(player));
    }

    @Override
    public PlayerDTO updatePlayer(Long id, PlayerCreateDTO playerCreateDTO) {
        Optional<Player> player = playerRepository.findById(id);
        if (player.isEmpty()) {
            throw new ProdigyException(
                HttpStatus.NOT_FOUND,
                "The given player was not found"
            );
        }
        playerMapper.updatePlayer(player.get(), playerCreateDTO);
        return playerMapper.mapPlayer(playerRepository.save(player.get()));
    }

}
