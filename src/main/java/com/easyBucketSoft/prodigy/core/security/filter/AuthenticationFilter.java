package com.easyBucketSoft.prodigy.core.security.filter;


import com.easyBucketSoft.prodigy.core.security.jwt.JwtService;
import com.easyBucketSoft.prodigy.core.security.service.UserPrincipalService;
import com.easyBucketSoft.prodigy.database.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.io.IOException;
import java.util.Collections;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final Logger LOGGER = LoggerFactory.getLogger(AuthenticationFilter.class);

    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;
    private final UserPrincipalService userDetailsService;
    private final ObjectMapper objectMapper;

    public AuthenticationFilter(AuthenticationManager authenticationManager, JwtService jwtService, UserPrincipalService userDetailsService, ObjectMapper objectMapper) {
        this.authenticationManager = authenticationManager;
        this.jwtService = jwtService;
        this.userDetailsService = userDetailsService;
        this.objectMapper = objectMapper;
        this.configPathFiltering("/users/login", HttpMethod.POST);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {
        try {

            var credentials = objectMapper.readValue(req.getInputStream(), UserCredentials.class);
            var token = new UsernamePasswordAuthenticationToken(
                credentials.username(),
                credentials.password(),
                Collections.emptyList()
            );
            return this.authenticationManager.authenticate(token);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication auth) throws IOException {
        User principal = (User) auth.getPrincipal();
        Cookie cookie = jwtService.generateToken(principal);
        this.writeResponse(response, principal, cookie);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        LOGGER.error(exception.getMessage());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    private void writeResponse(HttpServletResponse response, User principal, Cookie cookie) throws IOException {
        String json = this.objectMapper.writeValueAsString(userDetailsService.buildResponse(principal));
        response.setStatus(HttpServletResponse.SC_OK);
        response.addCookie(cookie);
        response.getWriter().write(json);
        response.flushBuffer();
    }

    private void configPathFiltering(String path, HttpMethod method) {
        var requestMatcher = new AntPathRequestMatcher(path, method.name());
        this.setRequiresAuthenticationRequestMatcher(requestMatcher);
    }

    public record UserCredentials(String username, String password) {}

}
