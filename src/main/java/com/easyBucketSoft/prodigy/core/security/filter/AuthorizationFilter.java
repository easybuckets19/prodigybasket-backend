package com.easyBucketSoft.prodigy.core.security.filter;

import com.easyBucketSoft.prodigy.core.security.jwt.JwtService;
import com.easyBucketSoft.prodigy.core.security.service.UserPrincipalService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.io.IOException;

public class AuthorizationFilter extends BasicAuthenticationFilter {
    private final Logger LOGGER = LoggerFactory.getLogger(AuthorizationFilter.class);

    private final JwtService jwtService;
    private final UserPrincipalService userDetailsService;
    private final ObjectMapper objectMapper;

    public AuthorizationFilter(AuthenticationManager authenticationManager, JwtService jwtService, UserPrincipalService userDetailsService, ObjectMapper objectMapper) {
        super(authenticationManager);
        this.jwtService = jwtService;
        this.userDetailsService = userDetailsService;
        this.objectMapper = objectMapper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        try {
            String token = jwtService.extractToken(request);
            if (token != null) {
                Claims claims = jwtService.decodeToken(token);
                UserDetails principal = userDetailsService.loadUserByUsername(claims.getSubject());
                var authToken = this.buildSecurityToken(principal);
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        } catch (UsernameNotFoundException | JwtException exception) {
            LOGGER.error(exception.getMessage());
            SecurityContextHolder.clearContext();
        }
        filterChain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken buildSecurityToken(UserDetails principal) {
        return new UsernamePasswordAuthenticationToken(
            principal,
            null,
            principal.getAuthorities()
        );
    }

}
