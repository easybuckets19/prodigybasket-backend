package com.easyBucketSoft.prodigy.core.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;

import javax.crypto.SecretKey;
import java.util.Date;

@Service
public class JwtServiceImpl implements JwtService {

    private final JwtProperties properties;

    public JwtServiceImpl(JwtProperties properties) {
        this.properties = properties;
    }

    @Override
    public String extractToken(HttpServletRequest request) {
        Cookie cookie = WebUtils.getCookie(request, this.properties.getCookie());
        return cookie != null ? cookie.getValue() : null;
    }

    @Override
    public Cookie generateToken(UserDetails details) {
        String token = this.buildToken(details);
        return this.buildCookie(
            properties.getCookie(),
            properties.getExpiration(),
            token
        );
    }

    @Override
    public Claims decodeToken(String token) {
        JwtParser parser = this.buildParser(properties.getSecretKey());
        return parser.parseClaimsJws(token).getBody();
    }

    private String buildToken(UserDetails details) {
        Date issued = new Date(System.currentTimeMillis());
        Date expiry = new Date(System.currentTimeMillis() + properties.getExpiration());
        return Jwts.builder()
            .setSubject(details.getUsername())
            .setIssuedAt(issued)
            .setExpiration(expiry)
            .signWith(properties.getSecretKey())
            .compact();
    }

    private Cookie buildCookie(String name, Integer age, String value) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(age);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        return cookie;
    }

    private JwtParser buildParser(SecretKey key) {
        return Jwts.parserBuilder()
            .setSigningKey(key)
            .build();
    }


}
