package com.easyBucketSoft.prodigy.core.security.jwt;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.crypto.SecretKey;

@ConfigurationProperties(prefix = "properties.jwt")
public class JwtProperties {

    private final String prefix;
    private final String header;
    private final Integer expiration;
    private final String cookie;
    private final SignatureAlgorithm algorithm;
    private final SecretKey secretKey;

    public JwtProperties(String prefix, String header, Integer expiration, String cookie, SignatureAlgorithm algorithm, SecretKey secretKey) {
        this.prefix = prefix;
        this.header = header;
        this.expiration = expiration;
        this.cookie = cookie;
        this.algorithm = algorithm;
        this.secretKey = Keys.secretKeyFor(algorithm);
    }

    public String getPrefix() {
        return prefix;
    }

    public String getHeader() {
        return header;
    }

    public Integer getExpiration() {
        return expiration;
    }

    public SignatureAlgorithm getAlgorithm() {
        return algorithm;
    }

    public String getCookie() {
        return cookie;
    }

    public SecretKey getSecretKey() {
        return secretKey;
    }

}
