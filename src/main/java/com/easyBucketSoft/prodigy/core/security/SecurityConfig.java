package com.easyBucketSoft.prodigy.core.security;

import com.easyBucketSoft.prodigy.core.security.filter.AuthenticationFilter;
import com.easyBucketSoft.prodigy.core.security.filter.AuthorizationFilter;
import com.easyBucketSoft.prodigy.core.security.jwt.JwtService;
import com.easyBucketSoft.prodigy.core.security.service.UserPrincipalService;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;


@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfig {
    private final UserPrincipalService userPrincipalService;
    private final ObjectMapper objectMapper;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;

    public SecurityConfig(UserPrincipalService userPrincipalService, ObjectMapper objectMapper, JwtService jwtService) {
        this.userPrincipalService = userPrincipalService;
        this.objectMapper = objectMapper;
        this.jwtService = jwtService;
        this.passwordEncoder = new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(List.of("*"));
        configuration.setAllowedMethods(List.of("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(List.of("authorization", "content-type", "x-auth-token"));
        configuration.setExposedHeaders(List.of("x-auth-token"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return this.passwordEncoder;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, AuthenticationConfiguration config) {
        try {
            this.configureCORS(http);
            this.configureExceptionHandling(http);
            this.configureSessionManagement(http);
            this.configureAllowedPaths(http);
            this.configureGlobalAuth(http);
            this.configureProvider(http);
            this.configureFilters(http, config);
            return http.build();
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    private void configureCORS(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable();
    }

    private void configureExceptionHandling(HttpSecurity http) throws Exception {
        http.exceptionHandling().authenticationEntryPoint((request, response, authException) -> {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        });
    }

    private void configureSessionManagement(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    private void configureAllowedPaths(HttpSecurity http) throws Exception {
       // http.authorizeHttpRequests().requestMatchers(HttpMethod.POST, "/users").permitAll();
    }

    private void configureGlobalAuth(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests().anyRequest().permitAll();
    }

    private void configureProvider(HttpSecurity http) {
        var provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(this.userPrincipalService);
        provider.setPasswordEncoder(passwordEncoder());
        http.authenticationProvider(provider);
    }

    private void configureFilters(HttpSecurity http, AuthenticationConfiguration config) throws Exception {
        var manager = config.getAuthenticationManager();
        http.addFilter(
            new AuthenticationFilter(
                manager,
                this.jwtService,
                this.userPrincipalService,
                this.objectMapper
            )
        );
        http.addFilter(
            new AuthorizationFilter(
                manager,
                this.jwtService,
                this.userPrincipalService,
                this.objectMapper
            )
        );
    }

}