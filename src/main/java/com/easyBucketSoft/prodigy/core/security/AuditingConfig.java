package com.easyBucketSoft.prodigy.core.security;

import com.easyBucketSoft.prodigy.core.security.service.UserAuditingService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditProvider")
public class AuditingConfig {
    private final UserAuditingService auditingService;

    public AuditingConfig(UserAuditingService auditingService) {
        this.auditingService = auditingService;
    }

    @Bean
    public AuditorAware<String> auditProvider() {
        return auditingService;
    }

}
