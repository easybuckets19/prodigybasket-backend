package com.easyBucketSoft.prodigy.core.security.jwt;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.core.userdetails.UserDetails;

public interface JwtService {

    String extractToken(HttpServletRequest request);

    Cookie generateToken(UserDetails details);

    Claims decodeToken(String token);

}
