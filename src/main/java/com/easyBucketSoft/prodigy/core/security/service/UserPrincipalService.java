package com.easyBucketSoft.prodigy.core.security.service;

import com.easyBucketSoft.prodigy.api.data.mapper.UserMapper;
import com.easyBucketSoft.prodigy.api.data.payload.user.UserDTO;
import com.easyBucketSoft.prodigy.database.model.User;
import com.easyBucketSoft.prodigy.database.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Optional;


@Service
public class UserPrincipalService implements org.springframework.security.core.userdetails.UserDetailsService {
    private final UserRepository repository;
    private final UserMapper mapper;

    public UserPrincipalService(UserRepository repository, UserMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = repository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException(
                MessageFormat.format(
            "The given user was not found.", username
                )
            );
        }
        return user.get();
    }

    public UserDTO buildResponse(User user) {
        return mapper.mapUser(user);
    }

}
